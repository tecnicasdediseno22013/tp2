package ar.fiuba.tecnicas.tetesteo;

import java.util.List;
import java.util.Map;

/**
 * Interfaz encargada de realizar el reporte del resultado
 * de los tests
 */
public interface TestReporter {

	public void report(TestResult result);

}
